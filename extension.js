const { Meta, St  } = imports.gi;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const WindowMenu = imports.ui.windowMenu;
const Clutter = imports.gi.Clutter;

// const list_commands = ["Minimize", "Maximize/Unmaximize",   "Resize", "Move Titlebar Onscreen", "Always On Top", "Always on Visible Workspace", "Move to Workspace Right", "Move to Workspace Up", "Move to Workspace Down", "Close"]; //both menu listing and return from _getMenuItems() are 10 elements so probably one to one mapping 
// const shortcutMap = new Map([    
//     ["m", _("Minimize")],
//     ["x", _("Maximize")],
//     ["x", _("Unmaximize")],
// 
//     ["v", _("Move")],
//     ["s", _("resize")],
// 		["t", _("Move Titlebar Onscreen")],	
// 
// 		["T", _("Always On Top")],//doesn't have shortcut
//     ["V", _("Always on Visible Workspace")],
//     
//     ["L", _("Move to Workspace Left")], //doesn't have shortcut
//     ["R", _("Move to Workspace Right")],
//     ["U", _("Move to Workspace Up")],	
//     ["D", _("Move to Workspace Down")],
// 
// 		["C", _("Close")]
// ]);
const shortcutMap = new Map([    
    [_("Minimize"), "m"],
    [_("Maximize"), "x"],
    [_("Unmaximize"), "x"],

    [_("Move"), "v"],
    [_("Resize"), "s"],
		[_("Move Titlebar Onscreen"), "T"],	

		[_("Always on Top"), "A"],//doesn't have shortcut
    [_("Always on Visible Workspace"), "V"],
    
    [_("Move to Workspace Left"), "L"], //doesn't have shortcut
    [_("Move to Workspace Right"), "R"],
    [_("Move to Workspace Up"), "U"],	
    [_("Move to Workspace Down"), "D"],
    
    [_("Move to Monitor Left"), "s"], //doesn't have shortcut
    [_("Move to Monitor Right"), "f"],
    [_("Move to Monitor Up"), "e"],	
    [_("Move to Monitor Down"), "a"],

		[_("Close"), "C"]
]);


const shortcutViewMap = new Map([    
    [_("Minimize"), "m"],
    [_("Maximize"), "x"],
    [_("Unmaximize"), "x"],

    [_("Move"), "o"],
    [_("Resize"), "s"],
		[_("Move Titlebar Onscreen"), "T"],	

		[_("Always on Top"), "p"],//doesn't have shortcut
    [_("Always on Visible Workspace"), "V"],
    
    [_("Move to Workspace Left"), "L"], //doesn't have shortcut
    [_("Move to Workspace Right"), "R"],
    [_("Move to Workspace Up"), "U"],	
    [_("Move to Workspace Down"), "D"],
    
    [_("Move to Monitor Left"), "s"], //doesn't have shortcut
    [_("Move to Monitor Right"), "f"],
    [_("Move to Monitor Up"), "e"],	
    [_("Move to Monitor Down"), "a"],

		[_("Close"), "C"]
]);
// const shortcutMap = new Map([    
//     [_("Minimize"), "m"],
//     [_("Maximize"), "x"],
//     [_("Unmaximize"), "x"],
// 
//     [_("Move"), "v"],
//     [_("Resize"), "s"],
// 		[_("Move Titlebar Onscreen"), "t"],	
// 
// 		[_("Always On Top"), "p"],//doesn't have shortcut
//     [_("Always on Visible Workspace"), "V"],
//     
//     [_("Move to Workspace Left"), "L"], //doesn't have shortcut
//     [_("Move to Workspace Right"), "R"],
//     [_("Move to Workspace Up"), "U"],	
//     [_("Move to Workspace Down"), "D"],
// 
// 		[_("Close"), "C"]
// ]);
// 
// 
// 
// const shortcutMap = new Map([    
//     ["Minimize", ["m"]],
//     ["Maximize", ["x"]],
//     ["Unmaximize", ["x"]],
// 
//     ["Move", ["v"]],
//     ["Resize", ["s"]],
// 		["Move Titlebar Onscreen"), "t"],	
// 
// 		["Always On Top"), "T"],//doesn't have shortcut
//     ["Always on Visible Workspace"), "V"],
//     
//     ["Move to Workspace Left"), "L"], //doesn't have shortcut
//     ["Move to Workspace Right"), "R"],
//     ["Move to Workspace Up"), "U"],	
//     ["Move to Workspace Down"), "D"],
// 
// 		["Close"), "C"]
// ]);
// 
// const shortcutMap2 = new Map([
//     ["m", 0],
// 
//     ["x", 1],
// 
//     ["e", 2],
// 
//     ["s", 3],
// 
//     ["v", 4],
//     ["V", 4],
//     
//     ["v", 5],
//     ["V", 5],
// 
//     ["v", 6],
//     ["V", 6],
// 
//     ["r", 7],
//     ["R", 7],
// 
//     ["u", 8],
//     ["U", 8],
// 
//     ["d", 9],
//     ["D", 9],
// 
// 		["c", 10],
//     ["C", 10]
// ]);
// 

const internalToExternalMap = new Map([    
    ["m", "Minimize"],
    ["x", "Maximize"],
    ["x", "Unmaximize"],

    ["v", "Move"],
    ["s", "resize"],
		["t", "Move Titlebar Onscreen"],	

		["A", "Always on Top"],
    ["V", "Always on Visible Workspace"],
    
    ["L", "Move to Workspace Left"], 
    ["R", "Move to Workspace Right"],
    ["U", "Move to Workspace Up"],	
    ["D", "Move to Workspace Down"],

    ["s", "Move to Monitor Left"], 
    ["f", "Move to Monitor Right"],
    ["e", "Move to Monitor Up"], 
    ["a", "Move to Monitor Down"],

		["C", "Close"]
]);

const inputKeyToInternalMap = new Map([    
// Min, Max toogle
    ["m", "m"],
    ["x", "x"],


// move resize and titlebar on screen
    ["o", "v"],
    ["s", "s"],
		["t", "t"],	


// Always on Top or Visible Workspace
		["p", "A"],
   	["p", "A"],
    
    ["v", "V"],
    ["V", "V"],



// move window around workspace    
    ["L", "L"], 
    ["l", "L"], 

    ["r", "R"],
    ["R", "R"],

    ["u", "U"],	
    ["U", "U"],	

    ["d", "D"],
    ["D", "D"],


// unverified monitor commands
    ["s", "s"], 
    ["f", "f"],
    ["e", "e"], 
    ["a", "a"],


		["c", "C"],
		["C", "C"]
]);


let originalMenuInit, originalBuildMenu;

function init() {
}

function enable() {
    originalMenuInit = PopupMenu.PopupMenu.prototype["_init"];
    PopupMenu.PopupMenu.prototype["_init"] = function() {
        originalMenuInit.apply(this, arguments);
        this.actor.connect('key-press-event', (actor, event) => {
				     // not sure what value r addes to this extension but it was stoping down from being interpreted by the menu. so removed
            //let r = this._onKeyPress.apply(this, [actor, event]);
            //log(r)
            //if (r == Clutter.EVENT_STOP) {
            //    return r;
            //}
            let items = this._getMenuItems();//list of items
				    let char_in = String.fromCharCode(event.get_key_symbol());
				   //log(event.get_key_symbol());
				    if(inputKeyToInternalMap.has(char_in)) {
								    let internal_rep = inputKeyToInternalMap.get(char_in);
								    let external_rep = internalToExternalMap.get(internal_rep);
				
				            // log(items);
								    for(let item of items){
																// log(item.label.text, item.shortcut);
																if(item.label.text == external_rep || item.shortcut == internal_rep){
																					item.activate(event);//just need an object with get_time method actually (can return 0)
																				   //log(item.label.text,item.shortcut);
																				   //log(internal_rep, external_rep);
																				  return Clutter.EVENT_STOP;
																}	
								    }			    
				    }
				    //log(event.get_key_symbol());
				    return Clutter.EVENT_PROPAGATE;
        });
    }

    originalBuildMenu = WindowMenu.WindowMenu.prototype['_buildMenu'];
    WindowMenu.WindowMenu.prototype['_buildMenu'] = function(window) {
      originalBuildMenu.apply(this, arguments);
      let items = this._getMenuItems();
      for (let pos in items) {
          let item = items[pos];

          let text = item.label.text;
				  //get rid of all the pervious stuff in the label and insert stuff that shows shortcut
				  // distroying didn't work
          // just having the replaceText function run didn't result in being able to use the arrow keys to select as well
         // if (shortcutMap.has(text)) {
  								//item.label.destroy_all_children();
  			//					replaceText(item.label, text, shortcutMap.get(text), shortcutViewMap.get(text) )  
  				//  }       
								
           	if (shortcutMap.has(text)) {
           	    // log(text);
				   	    // log(shortcutMap.get(text));

           	    let itemWithShortcut = new PopupMenuItemWithShortcut(text, shortcutMap.get(text), shortcutViewMap.get(text))

				   	    if (window.is_above() && text == "Always on Top"){
           	       itemWithShortcut.setOrnament(PopupMenu.Ornament.CHECK);
				   	    }
				   	    if (window.is_on_all_workspaces() && text == "Always on Visible Workspace"){
           	       itemWithShortcut.setOrnament(PopupMenu.Ornament.CHECK);
				   	    }

           	    itemWithShortcut.connect('activate', (m, e) => {
           	        item.emit('activate', e, m);
           	    });
           	    itemWithShortcut.connect('destroy', (m, e) => {
           	        item.emit('destory', e, m);
           	    });
           	    this.addMenuItem(itemWithShortcut, pos);
           	    this.box.remove_child(item.actor);
           	}      
      }
   }
}

function disable() {
    PopupMenu.PopupMenu.prototype["_init"] = originalMenuInit;
    WindowMenu.WindowMenu.prototype['_buildMenu'] = originalBuildMenu;
}

function replaceText(label, text, shortcut, shortcutView){     
				let i = text.indexOf(shortcutView);
        if (i >= 0) {
            label.add_child(new St.Label({ text: text.substring(0, i) }));
            label.add_child(new St.Label({ text: shortcutView, style_class: "menu-shortcut" }));
            label.add_child(new St.Label({ text: text.substring(i + 1) }));
        } else {
            label.add_child(new St.Label({ text: text }));
            label.add_child(new St.Label({ text: "(" + shortcutView + ")", style_class: "menu-shortcut" }));
        }

}
const PopupMenuItemWithShortcut = class extends PopupMenu.PopupBaseMenuItem {
    constructor(text, shortcut, shortcutView, params) {
        super(params);

        let label = new St.BoxLayout();
        let i = text.indexOf(shortcutView);
        if (i >= 0) {
            label.add_child(new St.Label({ text: text.substring(0, i) }));
            label.add_child(new St.Label({ text: shortcutView, style_class: "menu-shortcut" }));
            label.add_child(new St.Label({ text: text.substring(i + 1) }));
        } else {
            label.add_child(new St.Label({ text: text }));
            label.add_child(new St.Label({ text: "(" + shortcutView + ")", style_class: "menu-shortcut" }));
        }

        this.shortcut = shortcut;
        this.label = label;
        this.actor.add_child(this.label);
        this.actor.label_actor = this.label
    }

};

function getMethods(obj) {
  var result = [];
  for (var id in obj) {
    try {
      if (typeof(obj[id]) == "function") {
        result.push(id + ": " + obj[id].toString());
      }
    } catch (err) {
      result.push(id + ": inaccessible");
    }
  }
  return result;
}
