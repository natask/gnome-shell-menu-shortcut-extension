gnome-shell menu shortcut
=========================

This is a simple extension to bring back the shortcut keys to the gnome-shell menus. It seems the gnome-shell team made a concious decision to remove the menu shortcut key functionality from the menu in favour of direct shortcut key combinations. This can be considered a reasonable argument though in the years many of us have developed our own workflows relying on them, and this regression affects our productivity, for example, a quick sequence of "Alt+Space x" would maximize and unmaximize the window. More discussion can be found here: https://bugzilla.gnome.org/show_bug.cgi?id=733297

This extension CANNOT bring back all of your favorite shortcut keys, however, it gives you the flexibility of customizing any menu item with any shortcut keys by editing the shortcut key map.

Installation
------------
Currently, this extension has yet been uploaded to gnome-shell extensions website, so the current way of installing this extension is to download the project and save it in ~/.local/share/gnome-shell/extensions/
